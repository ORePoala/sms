#include "ofono.h"
#include <QtDBus>
#include "dbus/ofonoMessageManager.h"

Ofono::Ofono(QObject *parent) :
    QObject(parent)
{
    // m_MessageManager = new OfonoMessageManagerInterface("org.ofono", "/ril_0", QDBusConnection::systemBus());
}  

void Ofono::sendMessage(QString _to, QString _text){
    OfonoMessageManagerInterface* interface = new OfonoMessageManagerInterface(
        "org.ofono", "/ril_0", QDBusConnection::systemBus()
    );
    interface->SendMessage(_to, _text);
}

Ofono::~Ofono()
{
	// delete m_MessageManager;
}
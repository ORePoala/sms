
import QtQuick 2.1
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick.Controls 1.4

Kirigami.ApplicationWindow {
    id: root

    title: "SMS"

    width: 360
    height: 600
    
    pageStack.initialPage: mainPageComponent

    Component {
        id: mainPageComponent

        Kirigami.Page {
            id: pane
            title: "SMS"

            Rectangle{

                // id: pane
                color: "white"
                anchors.fill: parent
                Item {
                    Timer {
                        interval: 15000; running: true; repeat: true
                        onTriggered: {
                            MsgManager.sendMessage("+91", "Hi!")
                            time.text = "message sent"
                        }
                    }

                    Text { id: time   }
                }
                // ColumnLayout {
                //     spacing: 0
                //     anchors.fill: parent
                //     Label{
                //         text:  qsTr("Click button to send \"Hi!\"")
                //         color: "black"
                //         Layout.alignment: Qt.AlignCenter
                //     }
                //     Button{
                //         Layout.alignment: Qt.AlignCenter
                //         text: "Send"
                //         onClicked:{
                //             MsgManager.sendMessage("+918588049901", "Hi!")
                //         }
                //     }
                // }
            }
        }
    }
}


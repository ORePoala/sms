#ifndef OFONO_H
#define OFONO_H

#include "dbus/ofonoMessageManager.h"

class Ofono : public QObject
{
Q_OBJECT

public:
    Ofono(QObject *parent = 0);
    ~Ofono();

// private:
//     OfonoMessageManagerInterface* m_MessageManager;

public:
    Q_INVOKABLE void sendMessage(QString _to, QString _text);
};

#endif
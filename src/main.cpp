#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <QQuickView>
#include <QtDBus>
#include "dbus/ofonoMessageManager.h"
#include "ofono.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("KDE");
    QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("sms");

    QQuickView view;
    Ofono* MsgManager = new Ofono();
    // view.setSource(QUrl::fromLocalFile("qrc:///main.qml"));

    QQmlApplicationEngine engine;
    
    engine.rootContext()->setContextProperty("MsgManager", MsgManager);
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }
    int ret = app.exec();
    return ret;
}

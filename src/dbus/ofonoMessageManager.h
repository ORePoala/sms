#ifndef OFONOMESSAGEMANAGER_H
#define OFONOMESSAGEMANAGER_H

#include <QtDBus>
#include <QtCore>


class OfonoMessageManagerInterface: public QDBusAbstractInterface{
    Q_OBJECT

public:
    static inline const char *staticInterfaceName(){ return "org.ofono.MessageManager"; }

public:
    OfonoMessageManagerInterface(const QString &service, const QString &path,
            const QDBusConnection &connection, QObject *parent = 0);
    ~OfonoMessageManagerInterface();

public Q_SLOTS:
    QDBusPendingReply<QVariantMap> GetProperties(){
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("GetProperties"), argumentList);
    }
    QDBusPendingReply<QDBusObjectPath> SendMessage(const QString &_to, const QString &_text){
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(_to)<<QVariant::fromValue(_text);
        return asyncCallWithArgumentList(QLatin1String("SendMessage"), argumentList);
    }

Q_SIGNALS:
    // void PropertyChanged(const QString &_name, const QDBusVariant &_value);
    // void ImmediateMessage(const QString &_message, const QVariantMap &_info);
    // void IncomingMessage(const QString &_message, const QVariantMap &_info);
    // void MessageAdded(const QDBusObjectPath &_path, const QVariantMap &_properties);
    // void MessageRemoved(const QDBusObjectPath &_path);
};

namespace org {
    namespace ofono {
        typedef ::OfonoMessageManagerInterface MessageManager;
    }
}

#endif